module.exports = {
  plugins: [
    {
      resolve: "@thalhalla/gatsby-theme-thalhalla",
      options: {
        contentPath: "events",
        basePath: "/events",
      },
    },
  ],
}
