# gatsby-theme-thalhalla

Gatsby theme for the Thalhalla

## Notes

I followed the
[official builtding a theme tutorial](https://www.gatsbyjs.org/tutorial/building-a-theme/)
then got transitions working

there is a makefile for the lazy typist (me)

```
make d
```


## biblio

https://www.gatsbyjs.org/docs/adding-page-transitions-with-plugin-transition-link/#predefined-transitions

## Signing

Please sign your commits.  [Official git docs on signing](https://git-scm.com/book/en/v2/Git-Tools-Signing-Your-Work)
